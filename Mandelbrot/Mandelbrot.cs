﻿using System.Numerics;

namespace Mandelbrot
{
    class Mandelbrot
    {
        public static long EscapeTime(Complex c, long max)
        {
            Complex z = new Complex(0d, 0d);
            long i = 0;
            for (; i < max; i++)
            {
                z = Complex.Add(Complex.Multiply(z, z), c);
                // doing this instead of using z.Magnitude is WAY faster
                if (z.Real * z.Real + z.Imaginary * z.Imaginary >= 4d)
                {
                    break;
                }
            }
            // invert the color
            return max - i;
        }

        public static Complex PixelToPoint(int x, int y, int dimX, int dimY, Complex upperLeft, Complex lowerRight)
        {
            double width = lowerRight.Real - upperLeft.Real;
            double height = upperLeft.Imaginary - lowerRight.Imaginary;
            double re = upperLeft.Real + x * width / dimX;
            double im = upperLeft.Imaginary - y * height / dimY;
            return new Complex(re, im);
        }

        public static byte[] Render(int dimX, int dimY, Complex upperLeft, Complex lowerRight)
        {
            byte[] image = new byte[dimX * dimY];
            for (int y = 0; y < dimY; y++)
            {
                for (int x = 0; x < dimX; x++)
                {
                    image[y * dimX + x] = (byte) EscapeTime(PixelToPoint(x, y, dimX, dimY, upperLeft, lowerRight), 255);
                }
            }
            return image;
        }
    }
}
