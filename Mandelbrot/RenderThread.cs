﻿using System.Numerics;

namespace Mandelbrot
{
    class RenderThread
    {
        private int DimX { get; set; }
        private int DimY { get; set; }
        private Complex UpperLeft { get; set; }
        private Complex LowerRight { get; set; }
        public byte[] Result { get; set; }

        public RenderThread(int dimX, int dimY, Complex upperLeft, Complex lowerRight)
        {
            DimX = dimX;
            DimY = dimY;
            UpperLeft = upperLeft;
            LowerRight = lowerRight;
        }

        public void Render()
        {
            Result = Mandelbrot.Render(DimX, DimY, UpperLeft, LowerRight);
        }
    }
}
