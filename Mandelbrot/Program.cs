using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Threading;

namespace Mandelbrot
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage: Mandelbrot <Dimensions> <UpperLeft> <BottomRight> <filename>");
                Environment.Exit(1);
            }

            (int dimX, int dimY) = ((int, int)) ParsePair(args[0], 'x');
            Complex upperLeft = ParseComplex(args[1]);
            Complex lowerRight = ParseComplex(args[2]);
            string filename = args[3];
            RenderThread[] renderers = new RenderThread[Environment.ProcessorCount];
            Thread[] threads = new Thread[Environment.ProcessorCount];
            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                int newDimY = (dimY / Environment.ProcessorCount);
                RenderThread renderer = new RenderThread(dimX, newDimY, 
                    Mandelbrot.PixelToPoint(0, newDimY * i, dimX, dimY, upperLeft, lowerRight), 
                    Mandelbrot.PixelToPoint(dimX, newDimY * (i + 1), dimX, dimY, upperLeft, lowerRight));
                renderers[i] = renderer;
                Thread thread = new Thread(renderer.Render);
                thread.Start();
                threads[i] = thread;
            }

            byte[] bitmap = new byte[dimX * dimY];

            for (int i = 0; i < Environment.ProcessorCount; i++)
            {
                threads[i].Join();
                renderers[i].Result.CopyTo(bitmap, i * (dimX * dimY / Environment.ProcessorCount));
            }

            WriteImage(dimX, dimY, bitmap, filename);
        }

        static (double, double) ParsePair(string text, char delim)
        {
            int delimPos = text.IndexOf(delim);
            var tuple = (0d, 0d);
            if (Double.TryParse(text.Substring(0, delimPos), out tuple.Item1) && Double.TryParse(text.Substring(delimPos + 1), out tuple.Item2))
            {
                return tuple;
            }
            else
            {
                throw new FormatException(String.Format("Failed to parse \"{0}\"", text));
            }
        }

        static Complex ParseComplex(string text)
        {
            var tuple = ParsePair(text, ',');
            return new Complex(tuple.Item1, tuple.Item2);
        }

        static void WriteImage(int dimX, int dimY, byte[] arr, string filename)
        {
            ImageFormat format = null;
            string extension = Path.GetExtension(filename).ToLower();
            switch (extension)
            {
                case ".png":
                    format = ImageFormat.Png;
                    break;
                case ".jpg":
                case ".jpeg":
                    format = ImageFormat.Jpeg;
                    break;
                default:
                    Console.WriteLine("Format {0} not supported. Use png or jpg/jpeg", extension);
                    Environment.Exit(1);
                    break;
            }
            Bitmap bitmap = new Bitmap(dimX, dimY, PixelFormat.Format8bppIndexed);
            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, dimX, dimX), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            for (int i = 0; i < dimY; i++)
            {
                Marshal.Copy(arr, i * dimX, new IntPtr(data.Scan0.ToInt32() + i * dimX), dimX);
            }
            bitmap.UnlockBits(data);
            ColorPalette grayScalePallette = bitmap.Palette;
            for (int i = 0; i < grayScalePallette.Entries.Length; i++)
            {
                grayScalePallette.Entries[i] = Color.FromArgb(i, i, i);
            }
            bitmap.Palette = grayScalePallette;
            bitmap.Save(filename, format);
        }
    }
}
